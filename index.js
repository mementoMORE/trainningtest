'use strict';

class App {
    constructor (daysCount = 21, dataStorage = {}, weekIndex = 1) {
        this.daysCount = daysCount;
        this.dataStorage = dataStorage;
        this.weekIndex = weekIndex;
    }

    mainEvent() {
        if (!localStorage.getItem("dataStorage")){
            this.dataStorage = this.newData();
        } else {
            this.dataStorage = this.getStorage();
        }
        this.setDaysData(this.weekIndex);
        const previous = document.getElementById("prev");
        const next = document.getElementById("next");
        previous.addEventListener("mousedown", () => this.deductWeekIndex());
        next.addEventListener("mousedown", () => this.addWeekIndex());

        const reset = document.getElementById("reset-storage");
        reset.addEventListener("click", this.resetStorage);

        const elem = document.getElementById("days-block");
        elem.onclick = (e) => {
            let target = e.target;
            if (target.className === "day__content") {
                App.showModal(target.innerHTML,
                    (value) => {
                        target.innerHTML = value;
                        this.saveStorage();
                    }
                );
            }
        }
    }

    newData() {
        for (let i = 0; i < this.daysCount; i++) {
            this.dataStorage[i] = "";
        }
        localStorage.setItem("dataStorage", JSON.stringify(this.dataStorage));
        return JSON.parse(localStorage.getItem("dataStorage"));
    }

    getStorage () {
        return JSON.parse(localStorage.getItem("dataStorage"));
    }

    saveStorage () {
        const daysData = document.getElementsByClassName("day__content");
        for(let i = 7*this.weekIndex-7; i <7*this.weekIndex; i++) {
            this.dataStorage[i] = daysData[i%7].innerHTML;
        }
        localStorage.setItem("dataStorage", JSON.stringify(this.dataStorage));
    };

    setDaysData (week) {
        const daysData = document.getElementsByClassName("day__content");
        for (let i = (7*week)-7; i < 7*week; i++) {
            daysData[i%7].innerHTML = this.dataStorage[i];
        }
    }

    static showModal(value, callback) {
        App.showCover();
        const form = document.getElementById('form');
        const container = document.getElementById('form-container');
        form.elements.text.value = value;

        function complete(value) {
            App.hideCover();
            container.style.display = 'none';
            document.onkeydown = null;
            callback(value);
        }

        form.onsubmit = (event) => {
            event.preventDefault();
            let value = form.elements.text.value;
            complete(value);
            return false;
        };

        form.elements.cancel.onclick = function() {
            complete(value);
        };

        document.onkeydown = function(e) {
            if (e.keyCode === 27) {
                complete(value);
            }
        };

        container.style.display = 'block';
        form.elements.text.focus();
    }

    addWeekIndex() {
        const weekTitleNumber = document.getElementById("week_number");
        if (this.weekIndex < 3){
            this.weekIndex += 1;
            weekTitleNumber.innerHTML = this.weekIndex;
            this.setDaysData(this.weekIndex);
        }
    }

    deductWeekIndex() {
        const weekTitleNumber = document.getElementById("week_number");
        if (this.weekIndex > 1){
            this.weekIndex -= 1;
            weekTitleNumber.innerHTML = this.weekIndex;
            this.setDaysData(this.weekIndex);
        }
    }

    static showCover() {
        const coverDiv = document.createElement('div');
        coverDiv.id = 'bg_layer';
        document.body.appendChild(coverDiv);
    }

    static hideCover() {
        document.body.removeChild(document.getElementById('bg_layer'));
    }

    resetStorage () {
        localStorage.clear();
        this.dataStorage = () => this.newData();
        application.mainEvent();
    }
}

const application = new App();
application.mainEvent();
